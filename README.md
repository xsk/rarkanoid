# Rarkanoid

A clone of Arkanoid, in Rust, as an exercise.

It will contain a lot of comments, in the form of notes

## Limitations

- Should only use minifb in order to train in pixel manipulation. <https://docs.rs/minifb/0.15.1/minifb/>

## Learnings

- Pixel perfect collision is hard.
- Always keep in mnd that the ball can pass over an object if the angle is good enough.

## TODO

- End the game gracefully.
- Score board.
- Use only one "Framebuffer".
- Reduce the cpu consumption.

## LINKS

<https://github.com/emoon/rust_minifb>

## License

- [Apache License, Version 2.0]( http://www.apache.org/licenses/LICENSE-2.0.txt )
