extern crate minifb;

use minifb::{Key, Scale, Window, WindowOptions};

// Screen size, will be scaled to x2 later on using minifb
const WINDOW_WIDTH: usize = 640; // x
const WINDOW_HEIGHT: usize = 320; // y

// minifb - unsigned 32bit R-G-B, From the Documentation
//
// Updates the window with a 32-bit pixel buffer. The encoding for each pixel is `0RGB`:
// The upper 8-bits are ignored, the next 8-bits are for the red channel, the next 8-bits
// afterwards for the green channel, and the lower 8-bits for the blue channel.
//
// fn from_u8_rgb(r: u8, g: u8, b: u8) -> u32 {
//     let (r, g, b) = (r as u32, g as u32, b as u32);
//     (r << 16) | (g << 8) | b
// }
const WHITE: u32 = 0xFFFFFF;
const BLACK: u32 = 0x000000;

// Level Generation Stuff
//
// We only care about the first level yet
// TODO: Make an ending screen as well
const LEVEL_BORDERS: usize = 6;
const BLOCK_WIDTH: usize = 15;
const BLOCK_HEIGHT: usize = 6;
const Y_BLOCKS: usize = 10;
const X_BLOCKS: usize = 32;

// Player position and size Stuff
const PLAYER_Y: usize = 300;
const PLAYER_SIZE: usize = 30;
const PLAYER_SPEED: usize = 3;

/// Main function of the Rarkanoid game.
fn main() {
    // Pixel Vector "FrameBuffer" - #1
    let mut buffer: Vec<u32> = vec![0; WINDOW_WIDTH * WINDOW_HEIGHT]; // 204800 Size

    // Level area "FrameBuffer" - #2
    let mut blocks: Vec<u32> = vec![0; WINDOW_WIDTH * WINDOW_HEIGHT]; // 204800 Size
    let mut block_pos: Vec<u32> = vec![1; X_BLOCKS * Y_BLOCKS]; // 320 Blocks

    // Starting position of Player
    let mut player_x: usize = 300;

    // Starting position and speed params of Ball
    let mut ball_x: isize = 150;
    let mut ball_y: isize = 150;
    let mut ball_speed_x: isize = 2;
    let mut ball_speed_y: isize = 2;

    // Level ( y * WIDTH = one line + where you want to have a block )
    // First block, top left y = 20, x = 60
    //
    //  y1=20+(BLOCK_HEIGHT*[0..10])+[0..10]
    //   |
    //   |
    //  y2=20+((BLOCK_HEIGHT*[1..11])+[0..10])
    //

    // Fill the level first
    //
    //   ---- ----- ----- -----
    //   ---- ----- ----- -----
    //   ---- ----- ----- -----
    //
    for j in 0..Y_BLOCKS {
        for i in 0..X_BLOCKS {
            for y in 20 + (BLOCK_HEIGHT * j) + j..20 + ((j + 1) * BLOCK_HEIGHT + j) {
                for x in 60 + (i * BLOCK_WIDTH) + i..60 + BLOCK_WIDTH + (i * BLOCK_WIDTH) + i {
                    // We make sure that we fill in only the "active" blocks
                    blocks[(y * WINDOW_WIDTH) + x] = 1 * block_pos[(j * X_BLOCKS) + i];
                }
            }
        }
    }

    let mut window = Window::new(
        "Rarkanoid - Press ESC to exit",
        WINDOW_WIDTH,
        WINDOW_HEIGHT,
        WindowOptions {
            resize: false,
            scale: Scale::X2,
            ..WindowOptions::default()
        },
    )
    .expect("Error, Unable to Open Window");

    // Limiting fps to 50
    // Just to not spin up the cpu too muh
    // TODO: Find a better way for this
    window.limit_update_rate(Some(std::time::Duration::from_millis(20)));

    // Game main loop starts here
    while window.is_open() && !window.is_key_down(Key::Escape) && ball_y as usize <= WINDOW_HEIGHT {
        // Fill Level
        for y in 0..WINDOW_HEIGHT {
            for x in 0..WINDOW_WIDTH {
                if (x <= LEVEL_BORDERS || x >= WINDOW_WIDTH - LEVEL_BORDERS)
                    || y <= LEVEL_BORDERS
                    || blocks[(y * WINDOW_WIDTH) + x] == 1
                    || (x == ball_x as usize && y == ball_y as usize)
                {
                    buffer[(y * WINDOW_WIDTH) + x] = WHITE;
                } else {
                    buffer[(y * WINDOW_WIDTH) + x] = BLACK;
                }
            }
        }

        // Player, always same y position
        for x in player_x..(player_x + PLAYER_SIZE) {
            buffer[(PLAYER_Y * WINDOW_WIDTH) + x] = WHITE;
        }

        ball_x += ball_speed_x;
        ball_y += ball_speed_y;

        // collision with player
        if ball_y as usize == PLAYER_Y
            && (ball_x as usize >= player_x && ball_x as usize <= player_x + PLAYER_SIZE)
        {
            ball_speed_y *= -1;
        }

        // collision with borders on y
        if ball_y as usize == LEVEL_BORDERS {
            ball_speed_y *= -1;
        }

        // collision with borders on y
        if ball_x as usize >= (WINDOW_WIDTH - LEVEL_BORDERS) || ball_x as usize <= LEVEL_BORDERS {
            ball_speed_x *= -1;
        }

        // Lets do x collision first...
        // we only need to check for collisions, if ball_y is <= 90

        if ball_y <= 90 {
            //check if we hit something...
            if blocks[(ball_y as usize * WINDOW_WIDTH) + ball_x as usize] == 1 {
                // yup, we did
                // TODO : Fix this for all types of block collision
                println!("I got a hit at {}, {}", ball_x, ball_y);
                let hit_x = (ball_x - 60) / 16; // ball hit column ( based on block width )
                let hit_y = (ball_y - 26) / 7 + 1; // ball hit line ( based on block height )
                block_pos[(hit_y as usize * X_BLOCKS) + hit_x as usize] = 0;
                ball_speed_y *= -1;
            } else {
                // we did't
                println!("I did not got a hit at {}, {}", ball_x, ball_y);
                println!(
                    "Blocks value {}",
                    blocks[(ball_y as usize * WINDOW_WIDTH) + ball_x as usize]
                );
            }
        }

        // Update the Blocks
        for j in 0..Y_BLOCKS {
            for i in 0..X_BLOCKS {
                for y in 20 + (BLOCK_HEIGHT * j) + j..20 + ((j + 1) * BLOCK_HEIGHT + j) {
                    for x in 60 + (i * BLOCK_WIDTH) + i..60 + BLOCK_WIDTH + (i * BLOCK_WIDTH) + i {
                        // We make sure that we fill in only the "active" blocks
                        blocks[(y * WINDOW_WIDTH) + x] = 1 * block_pos[(j * X_BLOCKS) + i];
                    }
                }
            }
        }

        // Handle Player input
        window.get_keys().iter().for_each(|key| match key {
            Key::Right => player_x = move_right(player_x),
            Key::Left => player_x = move_left(player_x),
            _ => (),
        });

        // We unwrap here as we want this code to exit if it fails
        window
            .update_with_buffer(&buffer, WINDOW_WIDTH, WINDOW_HEIGHT)
            .unwrap();
    }
}

///
/// Sets the Player starting x drawing position to PLAYER_SPEED pixels more to the right.
///
fn move_right(mut player_x: usize) -> usize {
    if (player_x - PLAYER_SPEED) <= (WINDOW_WIDTH - PLAYER_SIZE - LEVEL_BORDERS) {
        player_x += PLAYER_SPEED;
    }

    player_x
}

///
/// Sets the Player starting x drawing position to PLAYER_SPEED pixels more to the left.
///
fn move_left(mut player_x: usize) -> usize {
    if (player_x - PLAYER_SPEED) > 0 {
        player_x -= PLAYER_SPEED;
    }

    player_x
}
